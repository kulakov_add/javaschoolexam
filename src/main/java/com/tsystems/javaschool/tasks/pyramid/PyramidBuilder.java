package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        }

        int lines = 1;
        while (true) {

            if (lines >= inputNumbers.size())
                throw new CannotBuildPyramidException();

            int result = lines * (lines + 1) / 2;
            if (result == inputNumbers.size()) {
                break;
            }
            if (result > inputNumbers.size())
                throw new CannotBuildPyramidException();
            lines++;
        }


        int size = inputNumbers.size();
        for(int i=0; i < size; i++){
            for(int j=1; j < (size-i); j++){
                if(inputNumbers.get(j-1) > inputNumbers.get(j)){
                    int temp = inputNumbers.get(j-1);
                    inputNumbers.set(j-1, inputNumbers.get(j));
                    inputNumbers.set(j, temp);
                }

            }
        }


        int columns = 2 * lines - 1;

        int[][] result = new int[lines][columns];

        int iterator = 0;
        for (int i = 0; i <= lines - 1 ; i++) {
            int pos = 0;
            for (int j = pos; pos < lines - i - 1; j++){
                result[i][j] = 0;
                pos++;
            }

            for (int j = 0; pos < columns - (lines - i - 1); j++)
                if (j % 2 == 0) {
                    result[i][pos] = inputNumbers.get(iterator);
                    iterator++;
                    pos++;
                } else {
                    result[i][pos] = 0;
                    pos++;
                }

            for (int j = pos; pos < columns; j++){
                result[i][j] = 0;
                pos++;
            }
        }

        return result;
    }


}
