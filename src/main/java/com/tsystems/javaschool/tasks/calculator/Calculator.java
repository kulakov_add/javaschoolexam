package com.tsystems.javaschool.tasks.calculator;
import static java.lang.Character.isDigit;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement == null || statement == "")
            return null;

        String [] errors = {"--", "++", "//", "**", "..", ","};
        for (String error:errors) {
            if (statement.contains(error))
                return null;
        }

        int oBrackets = 0;
        int cBrackets = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                oBrackets++;
            }
            if (statement.charAt(i) == ')') {
                cBrackets++;
                if (cBrackets > oBrackets)
                    return null;
            }
        }
        if (oBrackets != cBrackets)
            return null;

        StringBuffer bufStatement = new StringBuffer(statement);

        for (int i = 0; i < bufStatement.length(); i++) {
            if (bufStatement.charAt(i) == '(') {
                int index = bufStatement.indexOf(")", i);
                StringBuffer forEvaluation = new StringBuffer();
                for (int j = i + 1; j < index; j++) {
                    forEvaluation.append(bufStatement.charAt(j));
                }
                String yetAnotherBuffer = evaluate(forEvaluation.toString());
                bufStatement.replace(i, index + 1, yetAnotherBuffer);
            }
        }

        for (int i = 0; i < bufStatement.length(); i++) {
            if (bufStatement.charAt(i) == '/' || bufStatement.charAt(i) == '*') {
                StringBuffer left = new StringBuffer();
                StringBuffer right = new StringBuffer();
                int leftLimit = 0;
                int rightLimit = 0;
                boolean leftNegative = false;
                boolean rightNegative = false;

                for (int j = i - 1; j >= 0; j--){
                    if (isDigit(bufStatement.charAt(j)) || (bufStatement.charAt(j) == '.') ) {
                        left.append(bufStatement.charAt(j));
                        leftLimit = j;
                    } else if (bufStatement.charAt(j) == '-') {
                        if (j == 0 || !isDigit(bufStatement.charAt(j - 1))) {
                            leftNegative = true;
                            leftLimit = j;
                        }
                    }
                    else break;
                }

                for (int j = i + 1; j < bufStatement.length(); j++){

                    if (isDigit(bufStatement.charAt(j)) || (bufStatement.charAt(j) == '.') ) {
                        right.append(bufStatement.charAt(j));
                        rightLimit = j;
                    }else if (bufStatement.charAt(j) == '-' && j == i + 1) {
                        rightNegative = true;
                        rightLimit = j;
                    }
                    else break;
                }

                left = left.reverse();

                float bufResult = 0;

                if (Float.parseFloat(right.toString()) == 0)
                    return null;

                if (bufStatement.charAt(i) == '/') {
                    bufResult = Float.parseFloat(left.toString()) / Float.parseFloat(right.toString());
                }

                if (bufStatement.charAt(i) == '*') {
                    bufResult = Float.parseFloat(left.toString()) * Float.parseFloat(right.toString());
                }

                if(leftNegative ^ rightNegative) {
                    bufResult = -bufResult;
                }
                bufStatement.replace(leftLimit, rightLimit + 1, Float.toString(bufResult));

            }
        }

        for (int i = 0; i < bufStatement.length(); i++) {
            if (bufStatement.charAt(i) == '-' || bufStatement.charAt(i) == '+') {
                if (i == 0 || !isDigit(bufStatement.charAt(i - 1))) {
                    continue;
                }
                StringBuffer left = new StringBuffer();
                StringBuffer right = new StringBuffer();
                int leftLimit = 0;
                int rightLimit = 0;
                for (int j = i - 1; j >= 0; j--){

                    if (isDigit(bufStatement.charAt(j)) || (bufStatement.charAt(j) == '.') ) {
                        left.append(bufStatement.charAt(j));
                        leftLimit = j;
                    }else if (bufStatement.charAt(j) == '-') {
                        if (j == 0 || !isDigit(bufStatement.charAt(j - 1))) {
                            left.append(bufStatement.charAt(j));
                            leftLimit = j;
                        }
                    }
                    else break;
                }
                for (int j = i + 1; j < bufStatement.length(); j++) {
                    if (isDigit(bufStatement.charAt(j)) || (bufStatement.charAt(j) == '.')) {
                        right.append(bufStatement.charAt(j));
                        rightLimit = j;
                    } else break;
                }

                left = left.reverse();

                String bufResult = "";
                if (bufStatement.charAt(i) == '-') {
                    bufResult = Float.toString(Float.parseFloat(left.toString()) - Float.parseFloat(right.toString()));
                }

                if (bufStatement.charAt(i) == '+') {
                    bufResult = Float.toString(Float.parseFloat(left.toString()) + Float.parseFloat(right.toString()));
                }
                bufStatement.replace(leftLimit, rightLimit + 1, bufResult);

            }
        }

        boolean isFinal = true;
        float floatResult = 0;
        int intResult = 0;

        try {
            Float.parseFloat(bufStatement.toString());
        } catch(NumberFormatException e) {
            isFinal = false;
        } catch(NullPointerException e) {
            isFinal = false;
        }

        if (isFinal) {
            floatResult = (float) Math.round(Float.parseFloat(bufStatement.toString()) * 10000f) / 10000f;
            if (floatResult % 1 == 0) {
                intResult = (int) floatResult;
                return Integer.toString(intResult);
            }
            return Float.toString(floatResult);
        }
        return bufStatement.toString();
    }

}

